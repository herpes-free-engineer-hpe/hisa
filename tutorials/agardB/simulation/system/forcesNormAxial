/*--------------------------------*- C++ -*----------------------------------*\
|                                                                             |
|    HiSA: High Speed Aerodynamic solver                                      |
|    Copyright (C) 2014-2017 Johan Heyns - CSIR, South Africa                 |
|    Copyright (C) 2014-2017 Oliver Oxtoby - CSIR, South Africa               |
|                                                                             |
\*---------------------------------------------------------------------------*/

// Aerodynamic forces

forcesNormAxial
{
    type                forceCoeffs;
    libs                ( "libforces.so" );
    writeControl        timeStep;
    writeInterval       1;

    patches             ( body bodyBase );
    rho                 rhoInf;   // Required for rhoInf to be read
    log                 false;

    liftDir             (0 1 0);
    dragDir             (1 0 0);
    CofR                (0.89145 0 0);
    pitchAxis           (0 0 -1);

    magUInf             268.0;
    rhoInf              1.0;
    lRef                0.34641;
    Aref                0.155846; // Full model area
}


// ************************************************************************* //
